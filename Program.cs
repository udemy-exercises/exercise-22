﻿using System;
using System.IO;
using System.Collections.Generic;

class Program
{
   static void Main()
   {
      Console.Write("Enter file full path: ");
      string path = Console.ReadLine();

      string[] lines = File.ReadAllLines(path);

      Dictionary<string, Person> map = new Dictionary<string, Person>();

      for (int i = 0; i < lines.Length; i++)
      {
         var p = new Person();
         string[] input = lines[i].Split(',');
         
         if (map.ContainsKey(input[0]))
         {
            map[input[0]].Votes += int.Parse(input[1]);
         }

         else
         {
            p.Name = input[0];
            p.Votes = int.Parse(input[1]);

            map[input[0]] = p;
         }
      }

      foreach (var p in map)
      {
         Console.WriteLine(p.Value);
      }
   }
}