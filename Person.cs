class Person
{
   public string Name {get; set;}
   public int Votes {get; set;}

   public override string ToString()
   {
      return $"{this.Name},{this.Votes}";
   }
}